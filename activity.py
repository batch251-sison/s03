class Camper():
    def __init__(self, name, batch, course):
        self.name = name
        self.batch = batch
        self.course = course

    
    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.")
    
    def career_track(self):
        print(f"Currently enrolled at {self.course} program.")

zuitt_camper = Camper("Ludwig", "251", "Python shourt course")

print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course}")
zuitt_camper.info()
zuitt_camper.career_track()
